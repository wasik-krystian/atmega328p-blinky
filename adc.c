#include <avr/io.h>
#include "adc.h"

void adc_init(void)
{
    /* Enable ADC, set prescaler to 16 */
    ADCSRA = _BV(ADEN) | _BV(ADPS2);
}

#define MUX_MASK (0xf)
unsigned adc_conversion(unsigned channel)
{
    /* Set channel and use AVCC as reference */
    ADMUX = (channel & MUX_MASK) | _BV(REFS0);

    /* Start conversion */
    ADCSRA |= _BV(ADSC);

    /* Wait until conversion completes */
    while (ADCSRA & _BV(ADSC));

    /* We have to make sure to read ADCL first */
    uint8_t low  = ADCL;
    uint8_t high = ADCH;

    return (high << 8) | low;
}
