# atmega328p-blinky

## Prerequisites (Linux)

Install the following packages:

- gcc-avr
- avrdude
- avr-libc

## Building

In order to build, run:

`make`

To clean:

`make clean`

## Flashing

Run the following command (you may need to replace `/dev/ttyUSB0` with path to appropriate serial port):

`avrdude -q -q -patmega328p -carduino -P/dev/ttyUSB0 -b57600 -D -Uflash:w:blinky.hex:i`

Or use:

`./flash.sh <SERIAL_PORT> <HEX_FILE>`

Both params are optional. The defaults are `/dev/ttyUSB0` and `blinky.hex`.
