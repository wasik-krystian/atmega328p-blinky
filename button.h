#ifndef BUTTON_H
#define BUTTON_H

void button_init(void);
int button_is_pressed(void);

#endif /* BUTTON_H */
