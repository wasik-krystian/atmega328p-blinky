#include <avr/interrupt.h>
#include <string.h>

#include "led.h"
#include "button.h"
#include "uart.h"
#include "adc.h"

int main(void)
{
    int was_pressed = 0;

    led_init();
    uart_init(9600);
    button_init();
    adc_init();

    /* Enable interrupts */
    sei();

    uart_print("App started.\n\r");

    while (1)
    {
        /* No busy waiting. Only call uart_getline when we know it will return
         * immediately */
        if (uart_is_line_ready()) {

            char cmd[UART_RX_BUFFER_SIZE];
            uart_getline(cmd);

            if (strcmp(cmd, "on") == 0) {
                led_on();
            } else if (strcmp(cmd, "off") == 0) {
                led_off();
            } else if (strcmp(cmd, "conv") == 0) {
                unsigned sample = adc_conversion(0);
                uart_printf("sample = %u\n\r", sample);
            } else {
                uart_printf("Error: Unrecognized command [%s].\n\r", cmd);
            }
        }

        /* Since we're not busy waiting for commands, we can do other stuff... */
        int is_pressed = button_is_pressed();
        if (!was_pressed && is_pressed) {
            uart_print("Button pressed\n\r");
        }
        if (was_pressed && !is_pressed) {
            uart_print("Button released\n\r");
        }

        /* Save current state for later */
        was_pressed = is_pressed;
    }

    return 0;
}
