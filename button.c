#include <avr/io.h>
#include "button.h"

#define BTN_PORTx   PORTB
#define BTN_DDRx    DDRB
#define BTN_PINx    PINB
#define BTN_MASK    (1 << 1)

void button_init(void)
{
    /* Configure BTN pin as input with pull-up */
    BTN_DDRx &= ~BTN_MASK;
    BTN_PORTx |= BTN_MASK;
}

int button_is_pressed()
{
    int input_state = BTN_PINx & BTN_MASK;

    /* In this case pressing a button means shorting to GND,
     * therefore we have to negate */
    return !(input_state);
}
