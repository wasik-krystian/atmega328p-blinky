#include <avr/io.h>
#include "led.h"

#define LED_PORTx   PORTB
#define LED_DDRx    DDRB
#define LED_MASK    (1 << 5)

void led_init(void)
{
    /* Configure LED pin as output */
    LED_DDRx |= LED_MASK;
}

void led_on(void)
{
    LED_PORTx |= LED_MASK;
}

void led_off(void)
{
    LED_PORTx &= ~LED_MASK;
}
